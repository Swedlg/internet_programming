function removeAllTexts(){
	let elements = document.getElementsByClassName('inner-content');

	for (let i = 0; i < elements.length; i += 1) {
  		elements[i].style.display = "none";
	}
}

function openNews(){
	removeAllTexts();
 	document.getElementById('newsText').style.display = "block";
}

function openCatalog(){
	removeAllTexts();
 	document.getElementById('catalogText').style.display = "block";
}

function openMerch(){
	removeAllTexts();
 	document.getElementById('merchText').style.display = "block";
}

function openSupport(){
	removeAllTexts();
 	document.getElementById('supportText').style.display = "block";
}